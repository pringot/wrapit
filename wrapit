#!/bin/bash

set -u

myname=$(basename "$0")

function usage {
  echo "usage: ${myname} [stats|stop|rm|rmi|help|doc]"
}

function emit {
  local type=$1
  shift
  printf "${myname}[$type] %s\\n" "$*"
  tput sgr0
}

function info {
  tput setaf 2
  emit "info" "$*"
}

function warning {
  tput setaf 3
  emit "warning" "$*"
}

function error {
  tput setaf 1
  emit "error" "$*"
}

function die {
  error "$@"
  exit 1
}

function check_prereqs {
  for prog in docker realpath
  do
    command -v "${prog}" > /dev/null || die "${prog} is missing"
  done
}

check_prereqs

uid=$(id -u)
gid=$(id -g)
mapuid="${uid}:${gid}"
uname=$(id -un)
mypath=$(realpath "$(dirname "$0")")

function check_vars {
  if [ "${WRAPIT_BASE_IMAGE-}" = "" ] ; then
    die "The env var WRAPIT_BASE_IMAGE must be defined"
  fi
  if [ "${WRAPIT_BASE_TAG-}" = "" ] ; then
    die "The env var WRAPIT_BASE_TAG must be defined"
  fi
  if [ "${WRAPIT_SHARED_DIR-}" = "" ] ; then
    die "The env var WRAPIT_SHARED_DIR must be defined"
  fi
  if [ "${WRAPIT_MAX_MEM-}" = "" ] ; then
    die "The env var WRAPIT_MAX_MEM must be defined"
  fi
  if [ "${WRAPIT_MAX_CPUS-}" = "" ] ; then
    die "The env var WRAPIT_MAX_CPUS must be defined"
  fi
  if [ "${WRAPIT_ENV_NAME-}" = "" ] ; then
    die "The env var WRAPIT_ENV_NAME must be defined"
  fi
}

check_vars

if [ "${WRAPIT_RUNTIME_OPTS-}" = "" ] ; then
  runtime_opt=""
else
  runtime_opt="$WRAPIT_RUNTIME_OPTS"
fi

if [ "${WRAPIT_DOCKER_RUNTIME-}" = "nvidia" ] ; then
  runtime_opt="--runtime=nvidia"
else
  runtime_opt=""
fi

if [ "${WRAPIT_SHARED_PORT-}" = "" ] ; then
  shared_port_opt=""
else
  shared_port_opt="-p ${WRAPIT_SHARED_PORT}"
fi

maxmem="${WRAPIT_MAX_MEM}"
maxcpus="${WRAPIT_MAX_CPUS}"
suffix="${WRAPIT_ENV_NAME}"

iname="wrapit-${WRAPIT_BASE_IMAGE}-${uname}"
itag="${WRAPIT_BASE_TAG}"

cname="wrapit-${uname}-${suffix}"

function create_image {
 base_dockerfile="${mypath}/Dockerfile.tail"
 dockerdir="/tmp/wrapit.$$"
 wrapitlog="/tmp/wrapit.log.$$"
 mkdir "${dockerdir}"
 (
   cd "${dockerdir}" || exit 1
   (echo "FROM ${WRAPIT_BASE_IMAGE}:${WRAPIT_BASE_TAG}"; cat "${base_dockerfile}") > Dockerfile
   if docker build --build-arg UID="${uid}" --build-arg GID="${gid}" -t "${iname}:${itag}" . > "${wrapitlog}" 2>&1 ; then
     exit 0
   else
     cat "${wrapitlog}"
     exit 1
   fi
 )
 result=$?
 rm -rf "${dockerdir}" "${wrapitlog}"
 return $result
}

function test_image {
  docker images --format "{{.Repository}}:{{.Tag}}" | grep -q "${iname}:${itag}"
  return $?
}

function test_container {
  docker ps -aq --no-trunc --filter name="^/${cname}$" | grep -q .
  return $?
}

function test_container_status {
  docker ps -aq --no-trunc --filter name="^/${cname}$" --filter status="$1" | grep -q .
  return $?
}

function check_base_image_update {
  readarray -t history_of_base_image < <(docker history -q "${WRAPIT_BASE_IMAGE}:${WRAPIT_BASE_TAG}")
  len_base="${#history_of_base_image[*]}"
  readarray -t history_of_image < <(docker history -q "${iname}:${itag}")
  len_image="${#history_of_image[*]}"
  history_of_image=(${history_of_image[@]:$((len_image-len_base))})
  [ "${history_of_image[*]}" == "${history_of_base_image[*]}" ]
  return $?
}

if [ $# = 1 ] ; then
  case $1 in 
    rmi) 
      if test_image ; then
        if test_container ; then
          die "${iname}:${itag} is associated with ${cname}"
        else
          docker rmi "${iname}:${itag}"
          exit $?
        fi
      else
        die "${iname}:${itag} does not exist"
      fi
    ;;
    stats)
      if test_container ; then
        if test_container_status running ; then
          docker stats "${cname}"
          exit $?
        else
          die "${cname} is not running"
        fi
      else
        die "${cname} does not exist"
      fi
    ;;
    stop)
      if test_container ; then
        if test_container_status running ; then
          docker stop "${cname}"
          exit $?
        else
          die "${cname} is not running"
        fi
      else
        die "${cname} does not exist"
      fi
    ;;
    rm)
      if test_container ; then
        if test_container_status exited ; then
          docker rm "${cname}"
          exit $?
        else
          die "${cname} is not stopped"
        fi
      else
        die "${cname} does not exist"
      fi
    ;;
    help)
      usage
      exit 0
    ;;
    doc)
      cat "${mypath}/README.md"
      exit 0
    ;;
    *) 
     usage
     exit 1
    ;;
  esac
fi

if ! test_image ; then
  info "Creating the ${iname} docker image ..."
  if ! create_image ; then
    die "error building ${iname}"
  fi
fi

if test_container ; then
  if test_container_status exited ; then
    info "starting ${cname} container"
    if ! check_base_image_update ; then
      warning "${WRAPIT_BASE_IMAGE}:${WRAPIT_BASE_TAG} has changed !!!!! Please, start a new container"
    fi
    docker start -ai "${cname}"
  else
    if test_container_status running ; then
      info "connecting to running ${cname} container"
      docker exec -e WRAPIT_IN_EXEC=1 -it "${cname}" bash
    else
      info "check the status of the ${cname} container (don't know what to do)"
    fi
  fi
else
  info "running/creating ${cname} container"

  if ! check_base_image_update ; then
    warning "${WRAPIT_BASE_IMAGE}:${WRAPIT_BASE_TAG} has changed !!!!! Please, start a new container"
  fi

  mountdir="${WRAPIT_MOUNT_DIR-}"

  if [ "$mountdir" = "" ] ; then
    mountopt=""
  else
    mountopt="-v ${mountdir}:${mountdir}"
  fi

  # shellcheck disable=SC2086
  docker run -it --name "${cname}" \
    ${runtime_opt} \
    ${shared_port_opt} \
    -u "${mapuid}" \
    -v "${WRAPIT_SHARED_DIR}:/home/wrapit" \
    --env TZ=Europe/Paris \
    -m "${maxmem}" \
    --cpus "${maxcpus}" \
    --memory-swappiness 0 \
    ${mountopt} \
    "${iname}:${itag}" bash
fi
