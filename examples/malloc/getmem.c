#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

void main(int argc, char *argv[])
{
  long unsigned sz;
  if (argc != 2) {
    fprintf(stderr,"usage: getmem BYTES\n");
    exit(1);
  }
  sz=atol(argv[1]);
  fprintf(stdout,"Allocating %lu bytes\n",sz);
  char *a=malloc(sz);
  if (a==NULL) {
    fprintf(stderr,"getmem: error allocating memory\n");
    exit(1);
  }
  int i;
  for (i=0; i<sz;i++) {
    a[i]='p';
  }
  exit(0);
}
