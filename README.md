wrapit
======

A docker wrapper to limit memory & cpu usage of running processes on a docker host.

When running `wrapit`, a user specified directory is shared between the host
and the running (wrapped) container. File ownership is preserved within
the container (i.e: files created within the container belongs to the user).

The user can cap resources (memory, cpu) utilization (see the `WRAPIT_MAX_*`
variables below).

One can see `wrapit` as a light virtual machine dedicated to a particular usage,
and whose data and code perimeter is defined by two host-shared directories
(see the `WRAPIT_*_DIR` variables below).

`wrapit` can be used on a shared server to ensure that an experiment will not consume
all the cpu or memory resources without any limitation.

`wrapit` is [nvidia-docker](https://github.com/NVIDIA/nvidia-docker) compatible.

# Required environment variables

- `WRAPIT_BASE_IMAGE` the name of the wrapped image
- `WRAPIT_BASE_TAG` the tag of the wrapped image
- `WRAPIT_SHARED_DIR` the host data directory shared with the container
- `WRAPIT_MAX_MEM` the memory limit of the running container
- `WRAPIT_MAX_CPUS` the max number of cpus usable for the running container
- `WRAPIT_ENV_NAME` the unique name of the running container

# Optional environment variables
- `WRAPIT_MOUNT_DIR` a supplementary host directory to mount within the container using
the same path on both side
- `WRAPIT_DOCKER_RUNTIME` an argument to the `docker run --runtime` parameter (the only supported value is `nvidia`) - deprecated, use the more general `WRAPIT_RUNTIME_OPTS` variable instead
- `WRAPIT_RUNTIME_OPTS` argument to the `docker run` command
- `WRAPIT_SHARED_PORT` an argument to the `docker run -p` parameter

# Limitations

`wrapit` is usable only with Debian (Ubuntu) based images.

# Installation

Just clone this repository and alias the wrapit command:

``` console
john@doe:~/dev$ git clone https://gitlab.inria.fr/pringot/wrapit.git
Cloning into 'wrapit'...
remote: Counting objects: 94, done.
remote: Compressing objects: 100% (49/49), done.
remote: Total 94 (delta 41), reused 83 (delta 35)
Unpacking objects: 100% (94/94), done.
john@doe:~/dev$ echo "alias wrapit='$PWD/wrapit/wrapit'" >> $HOME/.bashrc
```

# Usage

``` console
john@doe:~/dev/wrapit/examples/matlab$ wrapit help
usage: wrapit [stats|stop|rm|rmi|help|doc]
```

When `wrapit` is called without argument, the environment variables above mentionned **must**
already be defined (usage of tools like `direnv` or `autoenv` is recommended).

The first time it is called for a set of variables, it creates a thin Docker image over the
`WRAPIT_BASE_IMAGE:WRAPIT_BASE_TAG` image. In brief, it adds in the wrapped image some usefull
packages (`sudo` for instance), it creates a `wrapit` user with the same `uid:gid` than the
running user, and it gives her the `sudo` rights.

Then a container is created from this extended image, and its running characteristics include what has been
provided by the `WRAPIT_*` variables.

If any one of these variables changes (for instance the amount of dedicated memory), the container must
be rebuilt. This is the purpose of the `rm` command of `wrapit`. It will remove the container, and the next
time `wrapit` is called, the container will be created using the new characteristics.

If the base image has changed, one has to delete the running container if any (using `wrapit rm`)
and then delete the wrapping image using the `rmi` command of `wrapit`. The next time `wrapit`
will be called, a new image will be created from the base image, and a new container will be
created and started.

The `stop` command of `wrapit` can be used to stop the running container without being connected to it, just like
a `docker stop`.

# Tricks

The `WRAPIT_SHARED_DIR` is the home directory of the `wrapit` user, so it is possible to use a `.bashrc` file (or another kind of rc file)
into this directory as if it were an actual homedir.

# Examples

## Matlab

In the Internal Loria Network, it is possible to NFS mount a ready to use Matlab
installation on the `/local/logiciels` folder.

``` console
john@doe:~/dev/wrapit/examples/matlab$ df /local/logiciels/
Sys. de fichiers     blocs de 1K   Utilisé Disponible Uti% Monté sur
mizar:/vol/logiciels  1020054784 347562688  672492096  35% /local/logiciels
```

We begin by building a docker image containing enough dependancies in order to
run this instance of Matlab. The `Dockerfile` used to build this image
is the following:

``` docker
FROM ubuntu:16.04

RUN apt-get update && apt-get install -qqy libx11-6 libxext6 libxt6
```

The image can be built using the `make` command and the provided `Makefile`:

``` console
john@doe:~/dev/wrapit/examples/matlab$ make matlab-image

john@doe:~/dev/wrapit/examples/matlab$ docker images
REPOSITORY             TAG                 IMAGE ID            CREATED             SIZE
mylab                  1.0                 6b5d4afa8e0e        4 seconds ago       157MB
```

Thanks to the `direnv` command (Ubuntu >=16.04) we can use a `.envrc` file in this
directory to set the required variables by `wrapit`:

``` bash
export WRAPIT_BASE_IMAGE=mylab
export WRAPIT_BASE_TAG=1.0
export WRAPIT_SHARED_DIR=$PWD
export WRAPIT_MAX_MEM=384m
export WRAPIT_MAX_CPUS=2
export WRAPIT_ENV_NAME=matlab-test
export WRAPIT_MOUNT_DIR=/local/logiciels
```

This file must be allowed the first time it is used:

``` console
john@doe:~/dev/wrapit/examples/matlab$ direnv allow
direnv: loading .envrc
direnv: export +WRAPIT_BASE_IMAGE +WRAPIT_BASE_TAG +WRAPIT_ENV_NAME +WRAPIT_MAX_CPUS +WRAPIT_MAX_MEM +WRAPIT_MOUNT_DIR +WRAPIT_SHARED_DIR
```

NB1: `direnv` is not required to run wrapit, only specific env vars are, but is
by far more convenient to use it.

NB2: on Ubuntu <= 14.04 or derivatives, one can use the `autoenv` package
instead of `direnv` (and a `.env`/`.env.leave` pair files instead of `.envrc`).

Now we can launch `wrapit`. This will build the wrapped image if it does not exist,
then it will launch the container associated with the environment:

``` console
john@doe:~/dev/wrapit/examples/matlab$ ../../wrapit
Creating the wrapit-mylab-john docker image ...
running/creating wrapit-john-matlab-test container
WARNING: Your kernel does not support swap limit capabilities or the cgroup is not mounted. Memory limited without swap.
wrapit@416717d336a1:~
```

Thanks to the local `.bashrc` file, we locate the matlab binary:

``` console
wrapit@416717d336a1:~$ cat .bashrc
alias matlab='/local/logiciels/MathWorks/linux-R2016A/bin/matlab -nodisplay'
```

We run a Matlab script which requires less than `WRAPIT_MAX_MEM`:

``` console
wrapit@416717d336a1:~$ matlab -r "run('./ok.m'); quit;"

                                                       < M A T L A B (R) >
                                             Copyright 1984-2016 The MathWorks, Inc.
                                             R2016a (9.0.0.370719) 64-bit (glnxa64)
                                                         April 13, 2016


To get started, type one of these: helpwin, helpdesk, or demo.
For product information, visit www.mathworks.com.


	Academic License

wrapit@416717d336a1:~$

```
All is fine. Then we run the script which requires more thant `WRAPIT_MAX_MEM`:

``` console
wrapit@416717d336a1:~$ matlab -r "run('./notok.m'); quit;"

                                                       < M A T L A B (R) >
                                             Copyright 1984-2016 The MathWorks, Inc.
                                             R2016a (9.0.0.370719) 64-bit (glnxa64)
                                                         April 13, 2016


To get started, type one of these: helpwin, helpdesk, or demo.
For product information, visit www.mathworks.com.


	Academic License

Killed
wrapit@416717d336a1:~$ dmesg | tail -2
[1058408.814117] Memory cgroup out of memory: Kill process 29028 (MATLAB) score 1384 or sacrifice child
[1058408.814158] Killed process 29028 (MATLAB) total-vm:2767524kB, anon-rss:387056kB, file-rss:154968kB
```
The `matlab` command is `Killed` by the "Out of Memory Killer" standard Linux
mechanism because it has crossed the limits established by wrapit (as specified
in the `WRAPIT_MAX_MEM` variable).

Exiting from the running container, we can see that it is associated with a wrapped image:

``` console
wrapit@416717d336a1:~$ exit

john@doe:~/dev/wrapit/examples/matlab$ docker images
REPOSITORY             TAG                 IMAGE ID            CREATED              SIZE
wrapit-mylab-john      1.0                 5158fd2a9d1d        About a minute ago   517MB
mylab                  1.0                 6b5d4afa8e0e        3 minutes ago        157MB
```
The container is still available for a run ("à la Docker Compose"):
``` console
john@doe:~/dev/wrapit/examples/matlab$ docker ps -a
CONTAINER ID        IMAGE                      COMMAND             CREATED             STATUS                      PORTS               NAMES
416717d336a1        wrapit-mylab-john:1.0      "bash"              18 minutes ago      Exited (0) 17 minutes ago                       wrapit-john-matlab-test
```

We can run it again, using the `wrapit` command:

``` console
john@doe:~/dev/wrapit/examples/matlab$ ../../wrapit
starting wrapit-john-matlab-test container
wrapit@416717d336a1:~$
```
